using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using UnityEditor;
using UnityEngine.XR.Interaction.Toolkit;

public class MeshDownloader : MonoBehaviour
{
    public string pythonExePath = @"path_to_python.exe";
    public string scriptPath = @"path_to_script.py";
    public string prompt = @"prompt";

    public IEnumerator CallPythonScript()
    {
        var filename = "test53";

        ProcessStartInfo psi = new ProcessStartInfo();
        psi.FileName = pythonExePath;
        psi.Arguments = $"\"{scriptPath}\" \"{prompt}\" \"{filename}\"";
        psi.UseShellExecute = false;
        psi.CreateNoWindow = true;
        psi.RedirectStandardOutput = true;
        psi.RedirectStandardError = true;

        using (Process process = Process.Start(psi))
        {
            string output = process.StandardOutput.ReadToEnd();
            string errors = process.StandardError.ReadToEnd();
            process.WaitForExit();

            Debug.Log("Output: " + output);
            if (!string.IsNullOrEmpty(errors))
            {
                Debug.LogError("Errors: " + errors);
            }
        }

        yield return LoadMeshFromFile($"Assets/OpenAIMeshes/{filename}.obj");
    }

    IEnumerator LoadMeshFromFile(string filePath)
    {
        AssetDatabase.ImportAsset(filePath, ImportAssetOptions.Default);
        Mesh mesh = (Mesh)AssetDatabase.LoadAssetAtPath(filePath, typeof(Mesh));

        GameObject meshObject = new GameObject("DownloadedMesh", typeof(MeshFilter), typeof(MeshRenderer));
        meshObject.GetComponent<MeshFilter>().mesh = mesh;
        meshObject.GetComponent<Renderer>().material = new Material(Shader.Find("Custom/MeshShader"));

        meshObject.transform.position = new Vector3((float)-2.625, (float)0.543, (float)0.0);

        yield return null;
    }
}