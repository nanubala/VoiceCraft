import os
import random
import string

import websocket
import uuid
import json
import urllib.request
import urllib.parse
import sys

from requests_toolbelt import MultipartEncoder


server_address = "api.voicecrafting.xyz"
AUTH_HEADER = "Basic dXNlcjpob2xtZXNjb25jdXI="
client_id = str(uuid.uuid4())

def queue_prompt(prompt):
    p = {"prompt": prompt, "client_id": client_id}
    data = json.dumps(p).encode('utf-8')
    req = urllib.request.Request("https://{}/prompt".format(server_address), data=data, headers={'Content-Type': 'application/json'})
    req.add_header("Authorization", AUTH_HEADER)
    try:
        with urllib.request.urlopen(req) as response:
            return json.loads(response.read())
    except urllib.error.HTTPError as e:
        print('HTTP Error:', e.code, 'Reason:', e.reason, file=sys.stderr)


def get_output_file(filename, subfolder, folder_type):
    data = {"filename": filename, "subfolder": subfolder, "type": folder_type}
    url_values = urllib.parse.urlencode(data)
    url = "http://{}/view?{}".format(server_address, url_values)
    req = urllib.request.Request(url)
    req.add_header("Authorization", AUTH_HEADER)
    with urllib.request.urlopen(req) as response:
        return response.read()

def get_history(prompt_id):
    url = "http://{}/history/{}".format(server_address, prompt_id)
    req = urllib.request.Request(url)
    req.add_header("Authorization", AUTH_HEADER)
    with urllib.request.urlopen(req) as response:
        return json.loads(response.read())

def get_meshes(ws, prompt):
    prompt_id = queue_prompt(prompt)['prompt_id']
    while True:
        out = ws.recv()
        if isinstance(out, str):
            message = json.loads(out)
            if message['type'] == 'executing':
                data = message['data']
                if data['node'] is None and data['prompt_id'] == prompt_id:
                    break  # Execution is done
        else:
            continue  # Previews are binary data

    history = get_history(prompt_id)[prompt_id]
    print(json.dumps(history, indent=4))  # Print the JSON sub-object to the terminal

    # Dynamically search for the first mesh output
    mesh_file_content = None
    for node_id, outputs in history['outputs'].items():
        if 'mesh' in outputs:  # Check if 'mesh' key is present in the outputs of this node
            mesh_details = outputs['mesh'][0]  # Get the first mesh details
            mesh_filename = mesh_details['filename']
            mesh_subfolder = mesh_details['subfolder']
            mesh_type = mesh_details['type']

            # Download the mesh file using the get_output_file function
            mesh_file_content = get_output_file(mesh_filename, mesh_subfolder, mesh_type)
            print(f"Downloaded mesh file: {mesh_filename}")  # Optionally print some details or handle the file
            break  # Stop after finding the first mesh entry

    return mesh_file_content  # Return the mesh file content directly or handle it as needed

def save_mesh_file(mesh_content, filename):
    # Generate a random 8-letter filename ending with '.obj'
    random_filename = filename + '.obj'
    # Define the path where the file will be saved
    file_path = os.path.join(os.getcwd(), "Assets/OpenAIMeshes", random_filename)  # Save in the current working directory

    # Write the mesh content to a file
    with open(file_path, 'wb') as file:
        file.write(mesh_content)
    print(f"Mesh file saved as: {file_path}")
    return file_path

def upload_wav(wav_path, name=None, image_type="input", overwrite=False):
    # Generate a random 8-letter filename ending with '.wav'
    if name is None:
        name = ''.join(random.choices(string.ascii_letters, k=8)) + '.wav'

    with open(wav_path, 'rb') as file:
        multipart_data = MultipartEncoder(
            fields={
                'image': (name, file, 'audio/wav'),
                'type': image_type,
                'overwrite': str(overwrite).lower()
            }
        )

        data = multipart_data
        headers = {'Content-Type': multipart_data.content_type, 'Authorization': AUTH_HEADER}
        req = urllib.request.Request("https://{}/upload/image".format(server_address), data=data, headers=headers)
        with urllib.request.urlopen(req) as response:
            return (name, response.read())
        
if __name__ == "__main__":
    prompt_text = sys.argv[1]
    filename = sys.argv[2]
    
    server_address = "api.voicecrafting.xyz"
    auth_header = "Basic dXNlcjpob2xtZXNjb25jdXI="
    client_id = str(uuid.uuid4())
    
    (upload_name, upload_response) = upload_wav("Assets/Scripts/voice.wav")

    with open("Assets/Scripts/VoicecraftAPIWorkflow.json", 'r') as file:
        prompt = json.load(file)
        prompt["47"]["inputs"]["audio"] = upload_name

    ws = websocket.WebSocket()
    ws.connect("wss://{}/ws?clientId={}".format(server_address, client_id),
            header={"Authorization": AUTH_HEADER})
    mesh = get_meshes(ws, prompt)

    if mesh:
        save_mesh_file(mesh, filename)
    else:
        print("No mesh file downloaded.", file=sys.stderr)